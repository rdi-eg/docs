# docs

You will find all sorts of documentations that do not relate to a particular project.

The actual docs are found in the wiki of this project.

## Current Documents
1. [Fork and Pipe for 4-Year-Olds and Up](https://gitlab.com/rdi-eg/docs/wikis/Fork,-Exec-and-Pipe-for-4-Year-Olds-and-Up)
1. [A Very Brief Introduction to CI and CD Without the Big Fancy Words](https://gitlab.com/rdi-eg/docs/wikis/A-Very-Brief-Introduction-to-CI-and-CD-Without-the-Big-Fancy-Words)
1. [How to Google Cloud Run](https://gitlab.com/rdi-eg/docs/wikis/How-to-Google-Cloud-Run)
1. [Google Compute Engine Documentation](https://gitlab.com/rdi-eg/docs/wikis/Google-Compute-Engine-Documentation)